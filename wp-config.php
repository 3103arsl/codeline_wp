<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'codeline_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'T_N]>F(2 Ozcw^Z(|KU[B%+Ra{$VDCH*~JK-9d9NGknHuK]gVS(U,uEl)$S*y1}/');
define('SECURE_AUTH_KEY',  '&fHi(GfCgG|&|APg1NhGfG$)bcG]LXt,dOIE-T ^-;8{]f(6=!g=eL>OMU>0gC:d');
define('LOGGED_IN_KEY',    '!F([EX]eD5qZ>/K]GD[r5#B(?3Wf_x]z1.^cV5VZnJ I*D2EI~)IWeeN5XC3Ics#');
define('NONCE_KEY',        'PV#>KoY1$nN=Dr>]_^g&bcu|H{hm)6lQ%L#*+!.V+*4nbH[5I=q~sf>i i*]1fry');
define('AUTH_SALT',        '3P^Kq4ELYsEUm21#s<:81g$URA>WEP,KbU]X[ZtSZiXw<LXCQ[T51/W3p/|vqq^$');
define('SECURE_AUTH_SALT', '_AJ]oPO3@fBxF![WFShYT<Kw6?<N=,c+Qkv@!xVJ|5!=mn0f%*!C=W[8 :.#B8w|');
define('LOGGED_IN_SALT',   ']:tZepnzaEQ5q)-vw%ycrYZ2gqD[P0%dP5K!6uTT4:?jyXHB{]xzPWu4x~_VygTr');
define('NONCE_SALT',       'C-xb1Inn810`2l})B&V7TQEZ!=QI6JZq}TE-;>{vQ7~I,YO*1c:z.fZ|>)|u.8K9');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'cdline_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
