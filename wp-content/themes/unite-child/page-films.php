<?php
/*
  Template Name: Films Template
 */

get_header();

$args = array(
    's' => $search,
    'post_type' => 'films',
    'posts_per_page' => 10,
);

$wp_query = new WP_Query($args);
//echo "<pre>";
//print_r($wp_query);
?>

<div id="primary" class="content-area col-sm-12 col-md-12">
    <main id="main" class="site-main" role="main">

        <?php while (have_posts()) : the_post(); ?>

            <?php get_template_part('content-films', 'page'); ?>

            <?php
            // If comments are open or we have at least one comment, load up the comment template
            if (comments_open() || '0' != get_comments_number()) :
                comments_template();
            endif;
            ?>

        <?php endwhile; // end of the loop.  ?>

    </main><!-- #main -->
</div><!-- #primary -->


<?php get_footer(); ?>