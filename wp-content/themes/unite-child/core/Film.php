<?php

class Film {

    private $prefix;
    private $post;
    private $postType;
    private $perPage;
    private $postMeta = array();

    public function __construct() {
        $this->prefix = 'bl_film';
        $this->postType = 'films';
        $this->register();
    }

    private function register() {
        add_action('init', array($this, 'bl_cp_register'));
        add_action('init', array($this, 'bl_ct_genre_register'));
        add_action('init', array($this, 'bl_ct_actor_register'));
        add_action('init', array($this, 'bl_ct_year_register'));
        add_action('init', array($this, 'bl_ct_country_register'));
        add_filter('rwmb_meta_boxes', array($this, 'film_register_meta_boxes'));
        add_shortcode('films', array($this, 'film_shortcode'));
    }

    public function bl_ct_genre_register() {

        /**
         * Taxonomy: Genre.
         */
        $labels = array(
            "name" => __('Genres'),
            "singular_name" => __('Genre'),
        );

        $args = array(
            "label" => __('Genres'),
            "labels" => $labels,
            "public" => true,
            "hierarchical" => false,
            "label" => 'Genres',
            "show_ui" => true,
            "show_in_menu" => true,
            "show_in_nav_menus" => true,
            "query_var" => true,
            "rewrite" => array('slug' => 'genre', 'with_front' => true,),
            "show_admin_column" => false,
            "show_in_rest" => false,
            "rest_base" => "",
            "show_in_quick_edit" => false,
        );
        register_taxonomy('genre', array($this->postType), $args);
    }

    public function bl_ct_actor_register() {

        /**
         * Taxonomy: Genre.
         */
        $labels = array(
            "name" => __('Actors'),
            "singular_name" => __('Actor'),
        );

        $args = array(
            "label" => __('Actors'),
            "labels" => $labels,
            "public" => true,
            "hierarchical" => false,
            "label" => 'Actors',
            "show_ui" => true,
            "show_in_menu" => true,
            "show_in_nav_menus" => true,
            "query_var" => true,
            "rewrite" => array('slug' => 'actor', 'with_front' => true,),
            "show_admin_column" => false,
            "show_in_rest" => false,
            "rest_base" => "",
            "show_in_quick_edit" => false,
        );
        register_taxonomy('actor', array($this->postType), $args);
    }

    public function bl_ct_year_register() {

        /**
         * Taxonomy: Genre.
         */
        $labels = array(
            "name" => __('Years'),
            "singular_name" => __('Year'),
        );

        $args = array(
            "label" => __('Years'),
            "labels" => $labels,
            "public" => true,
            "hierarchical" => false,
            "label" => 'Years',
            "show_ui" => true,
            "show_in_menu" => true,
            "show_in_nav_menus" => true,
            "query_var" => true,
            "rewrite" => array('slug' => 'year', 'with_front' => true,),
            "show_admin_column" => false,
            "show_in_rest" => false,
            "rest_base" => "",
            "show_in_quick_edit" => false,
        );
        register_taxonomy('year', array($this->postType), $args);
    }

    public function bl_ct_country_register() {

        /**
         * Taxonomy: Genre.
         */
        $labels = array(
            "name" => __('Countries'),
            "singular_name" => __('Country'),
        );

        $args = array(
            "label" => __('Countries'),
            "labels" => $labels,
            "public" => true,
            "hierarchical" => false,
            "label" => 'Countries',
            "show_ui" => true,
            "show_in_menu" => true,
            "show_in_nav_menus" => true,
            "query_var" => true,
            "rewrite" => array('slug' => 'country', 'with_front' => true,),
            "show_admin_column" => false,
            "show_in_rest" => false,
            "rest_base" => "",
            "show_in_quick_edit" => false,
        );
        register_taxonomy('country', array($this->postType), $args);
    }

    public function bl_cp_register() {
        /**
         * Post Type
         */
        $labels = array(
            "name" => __("Films"),
            "singular_name" => __("Film"),
        );

        $args = array(
            "label" => __("Films"),
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "publicly_queryable" => true,
            "show_ui" => true,
            "show_in_rest" => false,
            "rest_base" => "",
            "has_archive" => true,
            "show_in_menu" => true,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => array("slug" => "films", "with_front" => true),
            "query_var" => true,
            "supports" => array("title", "editor", "thumbnail", "excerpt", "revisions"),
        );

        register_post_type($this->postType, $args);
    }

    public function film_register_meta_boxes($meta_boxes) {
        $prefix = $this->prefix;
        $meta_boxes[] = array(
            'id' => $this->prefix . 'information',
            'title' => 'Information',
            'post_types' => $this->postType,
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'name' => 'Ticket Price',
                    'id' => $prefix . 'price',
                    'type' => 'number',
                ),
                array(
                    'name' => 'Release Date',
                    'id' => $prefix . 'release_date',
                    'type' => 'date',
                ),
            )
        );

        return $meta_boxes;
    }

    public function film_shortcode($atts, $content) {


        extract(shortcode_atts(array(// a few default values
            'posts_per_page' => '10',
            'caller_get_posts' => 1,
            'post__not_in' => get_option('sticky_posts'),
                        ), $atts));

        $atts['post_type'] = $this->postType;


        $posts = new WP_Query($atts);
        $output = '';
        if ($posts->have_posts()) {
            while ($posts->have_posts()):
                $post = $posts->the_post();
                setup_postdata($post);
                $output .= $this->htmlView($post);
            endwhile;
        }else {
            return; // no posts found
        }

        wp_reset_query();
        return html_entity_decode($output);
    }

    private function htmlView($post) {
        $output = '';
        $output .= '<ul>';
        $output .= '<li><a class="web-btn red" href = "' . get_the_permalink() . '">' . get_the_title() . '</a></li>';
        $output .= '</ul>';
        return $output;
    }

}

new Film();
